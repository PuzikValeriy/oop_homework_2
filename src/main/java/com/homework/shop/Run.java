package com.homework.shop;

import com.homework.phone.Phone;
import com.homework.phone.PhoneType;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Run {
    public static void main(String[] args) {
        Shop shop = new Shop();
        Phone phone1=new Phone("Nokia","e66",2,true,"steel", PhoneType.SLIDER);
        Phone phone2=new Phone("Samsung","s100",1,false,"white",PhoneType.BLOCK);
        Phone phone3=new Phone("Samsung","s100",1,false,"black",PhoneType.BLOCK);
        Phone phone4=new Phone("HTC","one",2,true,"yellow",PhoneType.SENSOR);
        Phone phone5=new Phone("Motorola","XT 1572",2,true,"black",PhoneType.SENSOR);
        Phone phone6=new Phone("Nokia","3310",1,false,"blue",PhoneType.BLOCK);
        Phone phone7=new Phone("SonyEricson","k750i",1,true,"red",PhoneType.BLOCK);
        Phone phone8=new Phone("Nokia","3310",1,false,"black",PhoneType.BLOCK);
        Phone phone9=new Phone("Samsung","g150",1,true,"black",PhoneType.FLIP);
        Phone phones[] = {phone1,phone2,phone3,phone4,phone5,phone6,phone7,phone8,phone9};

        shop.newDelivery(phones);
        shop.newDelivery(phones);
        shop.printPhonesInStock();
        System.out.println("");
        System.out.println("Найдено "+shop.countOfSearchPhone(phone8)+" телефона \n"+phone8.toString());
    }
}
