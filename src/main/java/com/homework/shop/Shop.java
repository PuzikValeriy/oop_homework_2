package com.homework.shop;

import com.homework.phone.Phone;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Shop {

    private Phone[] phonesInStock = new Phone[0];

    public Phone[] getPhonesInStock(){
        return phonesInStock;
    }

    public void newDelivery(Phone[] phonesDelivery){
    Phone[] result = new Phone[phonesInStock.length+phonesDelivery.length];
        if(phonesInStock.length==0){
            System.arraycopy(phonesDelivery, 0, result, 0, phonesDelivery.length);
        }
        else{
            for (int i = 0; i < result.length; i++) {
                if(i<phonesInStock.length) result[i]=phonesInStock[i];
                else result[i] = phonesDelivery[i-phonesInStock.length];
            }
        }
        phonesInStock=result;
    }

    public int countOfSearchPhone(Phone searchPhone){
        int count=0;
        for (Phone phone:phonesInStock){
            if(phone.equals(searchPhone)) count++;
        }
        return count;
    }

    public void printPhonesInStock(){
        for(Phone phone:phonesInStock){
            System.out.println(phone.toString());
        }
    }
}
