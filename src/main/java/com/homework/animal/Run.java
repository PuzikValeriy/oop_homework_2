package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Run {
    public static void main(String[] args) {
    Animal[] animals = {new Cat("Stewie","Orange"),new Lizard("Bart","Green"),
            new Salmon("Fisher","Aqua"),new Sparrow("Eagle","Grey")};
        for (Animal animal:animals){
            animal.voice();
        }
    }
}