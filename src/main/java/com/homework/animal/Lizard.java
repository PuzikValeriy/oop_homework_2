package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Lizard extends Animal implements Swimable,Runnable{
    Lizard(String name, String color) {
        this.type = Type.AMPHIBIAN;
        this.countOfLegs = 4;
        this.countOfWings = 0;
        this.color = color;
        this.name = name;
    }

    public String swim() {
        return "I can swim!";
    }
    public String run(){
        return "I can run!";
    }
    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ".I have " + countOfLegs +
                " legs. I have " + countOfWings + " wings. " + run()+" "+swim() + " Who am I?");

    }
}
