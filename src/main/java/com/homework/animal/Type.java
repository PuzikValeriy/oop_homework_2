package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public enum Type {
    MAMMAL,
    AMPHIBIAN,
    BIRD,
    FISH
}
