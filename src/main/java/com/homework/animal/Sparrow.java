package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Sparrow extends Animal implements Flyable {
    Sparrow(String name, String color) {
        this.type = Type.BIRD;
        this.countOfLegs = 2;
        this.countOfWings = 2;
        this.color = color;
        this.name = name;
    }

    public String fly() {
        return "I can fly!";
    }

    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ".I have " + countOfLegs +
                " legs. I have " + countOfWings + " wings. " + fly() + " Who am I?");

    }
}
