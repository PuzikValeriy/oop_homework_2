package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public abstract class Animal {
    public String name,color;
    public int countOfLegs, countOfWings;
    public Type type;

    public abstract void voice();
}
