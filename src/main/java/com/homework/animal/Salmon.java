package com.homework.animal;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Salmon extends Animal implements Swimable{
    Salmon(String name, String color) {
        this.type = Type.FISH;
        this.countOfLegs = 0;
        this.countOfWings = 0;
        this.color = color;
        this.name = name;
    }

    public String swim() {
        return "I can swim!";
    }

    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ".I have " + countOfLegs
                + " legs. I have " + countOfWings + " wings. " + swim() + " Who am I?");

    }
}
