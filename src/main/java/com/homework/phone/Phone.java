package com.homework.phone;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Phone {
    private String producer,model,color;
    private int numOfSim=1;
    private PhoneType type;
    private boolean hasCamera;

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setNumOfSim(int numOfSim) {
        this.numOfSim = numOfSim;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public void setHasCamera(boolean hasCamera) {
        this.hasCamera = hasCamera;
    }

    public Phone(String producer, String model, int numOfSim, boolean hasCamera, String color, PhoneType type){
        this.producer=producer;
        this.model=model;
        this.numOfSim=numOfSim;
        this.hasCamera=hasCamera;
        this.color=color;
        this.type=type;
    }

    @Override
    public int hashCode(){
        return ((producer.hashCode()+model.hashCode()+numOfSim+((hasCamera)?1:0)+type.ordinal())*44);
    }

    @Override
    public boolean equals(Object phone){
        return this.hashCode()==phone.hashCode();
    }

    @Override
    public String toString(){
        return ("Производитель: "+ producer + ". Модель: "+model+". Количество sim-карт: "+numOfSim+
                ". Наличие камеры: "+(hasCamera?"есть":"нету")+". Цвет: "+color+". Тип корпуса: "+type.toString()+".");
    }
}
