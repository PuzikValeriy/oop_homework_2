package com.homework.phone;

/**
 * Created by Valeriy on 09.10.2016.
 */
public enum PhoneType {
    SLIDER,
    BLOCK,
    SENSOR,
    FLIP
}
