package com.homework.phone;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Main {
    public static void main(String[] args) {
        Phone phone1=new Phone("Nokia","e66",2,true,"steel",PhoneType.SLIDER);
        Phone phone2=new Phone("Samsung","s100",1,false,"white",PhoneType.BLOCK);
        Phone phone3=new Phone("Samsung","s100",1,false,"black",PhoneType.BLOCK);
        System.out.println(phone1.toString());
        System.out.println(phone2.toString());
        System.out.println(phone3.toString());
        System.out.println(phone1.equals(phone2));
        System.out.println(phone2.equals(phone3));
    }
}
