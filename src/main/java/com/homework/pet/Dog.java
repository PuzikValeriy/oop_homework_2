package com.homework.pet;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Dog extends Pet{
    Dog(String name){
        this.name=name;
        this.type="Dog";
    }
    public void voice(){
        System.out.println("I am "+this.type+". My name is "+this.name+". My voice is woof-woof-woof!");
    }
}
