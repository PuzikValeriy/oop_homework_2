package com.homework.pet;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Cow extends Pet{
    Cow(String name){
        this.name=name;
        this.type="Cow";
    }
    public void voice(){
        System.out.println("I am "+this.type+". My name is "+this.name+". My voice is moo-moo-moo!");
    }
}
