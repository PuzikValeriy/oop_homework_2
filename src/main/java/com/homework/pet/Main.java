package com.homework.pet;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Barsik");
        cat.voice();
        Dog dog = new Dog("Rex");
        dog.voice();
        Cow cow = new Cow("Alex");
        cow.voice();
    }
}
