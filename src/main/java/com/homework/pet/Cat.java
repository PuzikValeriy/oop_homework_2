package com.homework.pet;

/**
 * Created by Valeriy on 09.10.2016.
 */
public class Cat extends Pet{
    Cat(String name){
        this.type="Cat";
        this.name=name;
    }
    public void voice(){
        System.out.println("I am "+this.type+". My name is "+this.name+". My voice is meow-meow-meow!");
    }
}
