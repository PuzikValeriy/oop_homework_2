package com.homework.pet;

/**
 * Created by Valeriy on 09.10.2016.
 */
public abstract class Pet {
    public String name, type;

    public abstract void voice();

}
